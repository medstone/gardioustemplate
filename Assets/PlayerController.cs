﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public Vector2 max;
	public Vector2 min;
	public Vector2 maxSpeed;
	Rigidbody shipRigid;
	public GameObject shotPrefab;
	public Vector3 shotSpawn = new Vector3(1.5f,0f,0f);

	// Use this for initialization
	void Start () {
		shipRigid = this.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 speed = Vector3.zero;
		if (Input.GetKey (KeyCode.W)) 
			speed.y += maxSpeed.y;
		if (Input.GetKey (KeyCode.S)) 
			speed.y -= maxSpeed.y;
		if (Input.GetKey (KeyCode.A)) 
			speed.x -= maxSpeed.x;
		if (Input.GetKey (KeyCode.D)) 
			speed.x += maxSpeed.x;
		if(Input.GetKeyDown(KeyCode.Space)){
			GameObject shot = Instantiate(shotPrefab) as GameObject;
			shot.GetComponent<Rigidbody>().MovePosition( new Vector3(this.transform.position.x + shotSpawn.x, this.transform.position.y + shotSpawn.y, 0));
		}
		shipRigid.MovePosition (new Vector3(Mathf.Min(Mathf.Max(shipRigid.position.x + speed.x,min.x),max.x), Mathf.Min(Mathf.Max(shipRigid.position.y + speed.y,min.y),max.y), 0));
	}
}
