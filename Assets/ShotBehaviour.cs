﻿using UnityEngine;
using System.Collections;

public class ShotBehaviour : MonoBehaviour {
	public float speed = .5f;

	// Use this for initialization
	void Start () {
		this.GetComponent<Rigidbody> ().velocity = new Vector3 (speed, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision coll){
		if (coll.gameObject.tag == "Enemy") {
			Destroy(coll.gameObject);
			Destroy(this.gameObject);
		}
	}
}
