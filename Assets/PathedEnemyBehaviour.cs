﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PathedEnemyBehaviour : MonoBehaviour {
	public Vector2 speed;
	Rigidbody enemyBod;
	enum movementState { forward = 0, diagonal, back};
	movementState curr = movementState.forward;
	public Vector2 pos1;
	public bool diagDown;
	Text score;
	public int points = 100;

	// Use this for initialization
	void Start () {
		enemyBod = this.GetComponent<Rigidbody> ();
		enemyBod.velocity = new Vector3 (-speed.x, 0f, 0f);
		score = GameObject.Find ("Score").GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.x <= pos1.x && curr == movementState.forward) {
			curr = movementState.diagonal;
			enemyBod.velocity = new Vector3 (speed.x, diagDown ? -speed.y : speed.y , 0f);
		} else if (((this.transform.position.y <= pos1.y && diagDown) || (this.transform.position.y >= pos1.y && !diagDown)) && curr == movementState.diagonal) {
			curr = movementState.back;
			enemyBod.velocity = new Vector3 (speed.x * 2, 0f, 0f);
		} else if (this.transform.position.x >= 10 && curr == movementState.back) {
			Destroy(this.gameObject);
		}
	}

	void OnDestroy(){
		score.text = (int.Parse (score.text) + points).ToString();
	}
}
